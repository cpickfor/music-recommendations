#!/usr/bin/env python3

from _album_database import _album_database
import unittest

class TestMovieDatabase(unittest.TestCase):
	def setUp(self):
		self.adb = _album_database()
		self.adb.load_albums('test.dat')

	def map_rankings_to_albums(self, rankings):
		return [self.adb.albums[ranking] for ranking in rankings]

	def test_00_length(self):
		self.assertEqual(len(self.adb.albums), 4)

	def test_01_albums_by_artist(self):
		albums1 = list(self.adb.GET_ALBUMS_BY_ARTIST('Artist 1'))
		self.assertEqual(albums1, self.map_rankings_to_albums([1, 2]))

		albums2 = list(self.adb.GET_ALBUMS_BY_ARTIST('Nonexistant'))
		self.assertEqual(albums2, [])

	def test_02_albums_by_year(self):
		albums1 = list(self.adb.GET_ALBUMS_BY_YEAR(2000))
		self.assertEqual(albums1, self.map_rankings_to_albums([1, 3]))

		albums2 = list(self.adb.GET_ALBUMS_BY_YEAR(0))
		self.assertEqual(albums2, [])

	def test_03_albums_by_decade(self):
		albums1 = list(self.adb.GET_ALBUMS_BY_DECADE('00'))
		self.assertEqual(albums1, self.map_rankings_to_albums([1, 3, 4]))

		albums2 = list(self.adb.GET_ALBUMS_BY_DECADE('Nonexistant'))
		self.assertEqual(albums2, [])

	def test_04_albums_by_genre(self):
		albums1 = list(self.adb.GET_ALBUMS_BY_GENRE('Genre 1'))
		self.assertEqual(albums1, self.map_rankings_to_albums([1, 4]))

		albums2 = list(self.adb.GET_ALBUMS_BY_GENRE('Nonexistant'))
		self.assertEqual(albums2, [])

	def test_05_all_artists(self):
		artists = self.adb.GET_ALL_ARTISTS()
		self.assertEqual(artists, set(('Artist 1', 'Artist 2', 'Artist 3')))

	def test_06_all_genres(self):
		artists = self.adb.GET_ALL_GENRES()
		self.assertEqual(artists, set(('Genre 1', 'Genre 2', 'Genre 5', 'Genre 7')))

	def test_07_all_subgenres(self):
		artists = self.adb.GET_ALL_SUBGENRES()
		self.assertEqual(artists, set(('Genre 1', 'Genre 3', 'Genre 4', 'Genre 8', 'Genre 9')))

	def test_08_sort_by(self):
		albums1 = list(self.adb.SORT_BY('year'))
		self.assertEqual(albums1, self.map_rankings_to_albums([2, 1, 3, 4]))


if __name__ == '__main__':
	unittest.main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
