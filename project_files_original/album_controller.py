from _album_database import _album_database
import json
import cherrypy

class AlbumController(object):
    def __init__(self, albums_file):
        self.mydb = _album_database()
        self.mydb.load_albums(albums_file)

    def GET_ALL_ALBUMS(self):
        output = {'result': 'success'}

        output['albums'] = [album.get_json() for album in self.mydb.albums.values()]

        return json.dumps(output)

    def GET_ALBUMS_BY_ARTIST(self, artist):
        output = {'result': 'success'}

        try:
            albums = []
            for a in self.mydb.GET_ALBUMS_BY_ARTIST(artist):
                albums.append(a.get_json())

            if len(albums) == 0:
                raise Exception('bad')

            output['albums'] = albums
        except Exception as ex:
            output['result'] = 'error'

        return json.dumps(output)

    def GET_ALBUMS_BY_GENRE(self, genre):
        output = {'result': 'success'}
        try:
            albums = []
            for a in self.mydb.GET_ALBUMS_BY_GENRE(genre):
                albums.append(a.get_json())
            if len(albums) == 0:
                raise Exception('bad')
            output['albums'] = albums
        except Exception as ex:
            output['result'] = 'error'

        return json.dumps(output)

    def GET_ALBUMS_BY_DECADE(self, decade):
        output = {'result': 'success'}
        try:
            albums = []
            for a in self.mydb.GET_ALBUMS_BY_DECADE(decade):
                albums.append(a.get_json())
            if len(albums) == 0:
                raise Exception('bad')
            output['albums'] = albums
        except Exception as ex:
            output['result'] = 'error'

        return json.dumps(output)

    def GET_ALBUMS_BY_YEAR(self, year):
        output = {'result': 'success'}
        try:
            albums = []
            for a in self.mydb.GET_ALBUMS_BY_YEAR(year):
                albums.append(a.get_json())
            if len(albums) == 0:
                raise Exception('bad')
            output['albums'] = albums
        except Exception as ex:
            output['result'] = 'error'

        return json.dumps(output)

    def GET_ALL_ARTISTS(self):
        output = {'result': 'success'}

        artists = []
        for artist in self.mydb.GET_ALL_ARTISTS():
            artists.append(artist)
        output['artists'] = artists

        return json.dumps(output)

    def GET_ALL_GENRES(self):
        output = {'result': 'success'}
        
        genres = []
        for genre in self.mydb.GET_ALL_GENRES():
            genres.append(genre)
        output['genres'] = genres

        return json.dumps(output)

    def GET_ALBUMS_BY_RANKING(self, ranking):
        output = {'result': 'success'}
        try:
            albums = []
            for a in self.mydb.GET_ALBUMS_BY_RANKING(ranking):
                albums.append(a.get_json())
            if len(albums) == 0:
                raise Exception('bad') 
            output['albums'] = albums
        except Exception as ex:
            output['result'] = 'error'
        return json.dumps(output)

    def GET_ALBUMS_BY_LIMIT(self,limit):
        output = {'result': 'success'}
    
        try:
            albums = []
            for a in self.mydb.GET_ALBUMS_BY_RANK_LIMIT(limit):
                albums.append(a.get_json())
            if len(albums) == 0:
                raise Exception('bad')
            output['albums'] = albums
        except Exception as ex:
            output['result'] = 'error'
        return json.dumps(output)


    def GET_FAVORITE_ALBUMS(self, user_id):
        output = {'result': 'success'}


        user_id = str(user_id) 

        try:
            albums = []
            for a in self.mydb.favorites[user_id]:
                albums.append(a)
            if len(albums) == 0:
                raise Exception('it didnt quite work')
            output['albums'] = albums
        except Exception as ex:
            output['result'] = 'error'

        return json.dumps(output)
    
    def DELETE_LIKE_FOR_USER(self):
        output = {'result':'success'}
        data = cherrypy.request.body.read()
        data = json.loads(data)
        
        try:
            self.mydb.DELETE_LIKED_ALBUM(data['title'],str(data['user_id']))
        except Exception as ex:
            output['message'] = ex
            output['result'] = 'error'
        return json.dumps(output)

    def PUT_LIKE_FOR_USER(self):
        output = {'result': 'success'}
        data = cherrypy.request.body.read()
        data = json.loads(data)
        try:
            self.mydb.PUT_LIKED_ALBUM(data['title'], str(data['user_id']))
        except Exception as ex:
            output['message'] = ex
            output['result'] = 'error'
        return json.dumps(output)

    def POST_ALBUM(self):
        output = {'result': 'success'}
        data = cherrypy.request.body.read()
        try:
            self.mydb.ADD_ALBUM(data)
        except Exception as ex:
            output['message'] = ex
            output['result'] = 'error'
        return json.dumps(output)

    def RESET(self):
        output = {'result':'success'}
        try:
            self.mydb.albums = {} # reset database
            self.mydb.load_albums('albums.dat')
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:

