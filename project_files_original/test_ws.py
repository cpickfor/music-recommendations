#!/escnfs/home/csesoft/2017-fall/anaconda3/bin/python3

import unittest
import requests
import json
import sys

port = 51047

class TestMovieDatabase(unittest.TestCase):
    BASE_URL = 'http://student04.cse.nd.edu:{}'.format(port)

    def setUp(self):
        resp = requests.put(self.BASE_URL + '/albums/reset', '')
        data = self.parse_result(resp)
        self.assertEqual(data['result'], 'success')

    def parse_result(self, response):
        # if it's not valid json, this will throw an exception and the test will fail
        return json.loads(response.content.decode('utf-8'))

    def test_00_get_albums(self):
        # These endpoints just return the result of a call to a database function,
        # so there's no need to check the contents because test_api checks that.
        # We can just check the result status and length
        endpoints = (
            ('/albums', 500),
            ('/albums/artists/The%20Beatles', 10),
            ('/albums/genres/Classical', 1),
            ('/albums/decade/70', 186),
            ('/albums/year/1969', 22),
            ('/albums/200', 200),
            ('/albums/ranking/200', 1),
        )

        for endpoint, length in endpoints:
            with self.subTest(endpoint=endpoint):
                resp = requests.get(self.BASE_URL + endpoint)
                data = self.parse_result(resp)
                self.assertEqual(data['result'], 'success')
                self.assertEqual(len(data['albums']), length)

    def test_01_get_artists(self):
        resp = requests.get(self.BASE_URL + '/artists')
        data = self.parse_result(resp)
        self.assertEqual(data['result'], 'success')
        self.assertEqual(len(data['artists']), 289)

    def test_02_get_genres(self):
        resp = requests.get(self.BASE_URL + '/genres')
        data = self.parse_result(resp)
        self.assertEqual(data['result'], 'success')
        self.assertEqual(len(data['genres']), 14)

    def test_03_post_album(self):
        album = {"year": 2019, "decade": "10", "genres":["Rock"],"subgenres":["Funk / Soul"], "artist": "The Lyons Cubs", "title": "Into the Lyons Den"}
        resp = requests.post(self.BASE_URL + '/albums', json.dumps(album))
        data = self.parse_result(resp)
        self.assertEqual(data['result'], 'success')

        # Make sure new album is in database
        resp = requests.get(self.BASE_URL + '/albums')
        data = self.parse_result(resp)
        self.assertEqual(data['result'], 'success')
        self.assertEqual(len(data['albums']), 501)

    def test_04_put_album_like(self):
        payload = {"title": "Goodbye Yellow Brick Road", "user_id": "1"}
        resp = requests.put(self.BASE_URL + '/albums', json.dumps(payload))
        data = self.parse_result(resp)
        self.assertEqual(data['result'], 'success')

        resp = requests.get(self.BASE_URL + '/albums/favorites/1')
        data = self.parse_result(resp)
        self.assertEqual(data['result'], 'success')
        self.assertEqual(len(data['albums']), 1)

if __name__ == '__main__':
    unittest.main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
