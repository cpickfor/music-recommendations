from album_controller import AlbumController
import cherrypy
import sys

class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

def start_service(port):
    mycon = AlbumController('albums.dat')

    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    dispatcher.connect('GET_ALL_ALBUMS', '/albums', controller=mycon, action='GET_ALL_ALBUMS', conditions=dict(method=['GET']))
    dispatcher.connect('GET_ALBUMS_BY_ARTIST', '/albums/artists/:artist', controller=mycon, action='GET_ALBUMS_BY_ARTIST', conditions=dict(method=['GET']))
    dispatcher.connect('GET_ALBUMS_BY_GENRE', '/albums/genres/:genre', controller=mycon, action='GET_ALBUMS_BY_GENRE', conditions=dict(method=['GET']))
    dispatcher.connect('GET_ALBUMS_BY_DECADE', '/albums/decade/:decade', controller=mycon, action='GET_ALBUMS_BY_DECADE', conditions=dict(method=['GET']))
    dispatcher.connect('GET_ALBUMS_BY_YEAR', '/albums/year/:year', controller=mycon, action='GET_ALBUMS_BY_YEAR', conditions=dict(method=['GET']))
    dispatcher.connect('GET_ALL_ARTISTS', '/artists', controller=mycon, action='GET_ALL_ARTISTS', conditions=dict(method=['GET']))
    dispatcher.connect('GET_ALL_GENRES', '/genres', controller=mycon, action='GET_ALL_GENRES', conditions=dict(method=['GET']))
    dispatcher.connect('GET_ALBUMS_BY_RANKING', '/albums/ranking/:ranking', controller=mycon, action='GET_ALBUMS_BY_RANKING', conditions=dict(method=['GET']))
    dispatcher.connect('GET_FAVORITE_ALBUMS', '/albums/favorites/:user_id', controller=mycon, action='GET_FAVORITE_ALBUMS', conditions=dict(method=['GET']))
    dispatcher.connect('GET_ALL', '/albums/:limit', controller=mycon, action='GET_ALBUMS_BY_LIMIT', conditions=dict(method=['GET']))
    dispatcher.connect('PUT_LIKE_FOR_USER', '/albums', controller=mycon, action='PUT_LIKE_FOR_USER', conditions=dict(method=['PUT']))
    dispatcher.connect('DELETE_LIKE_FOR_USER', '/albums/delete', controller=mycon, action='DELETE_LIKE_FOR_USER', conditions=dict(method=['PUT']))
    dispatcher.connect('POST_ALBUM', '/albums', controller=mycon, action='POST_ALBUM', conditions=dict(method=['POST']))
    dispatcher.connect('RESET_ALBUM', '/albums/reset', controller=mycon, action='RESET', conditions=dict(method=['PUT']))

    dispatcher.connect('GET_ALL_ALBUMS_OPTIONS', '/albums', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('GET_ALBUMS_BY_ARTIST_OPTIONS', '/albums/artists/:artist', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('GET_ALBUMS_BY_GENRE_OPTIONS', '/albums/genres/:genre', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('GET_ALBUMS_BY_DECADE_OPTIONS', '/albums/decade/:decade', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('GET_ALBUMS_BY_YEAR_OPTIONS', '/albums/year/:year', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('GET_ALL_ARTISTS_OPTIONS', '/artists', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('GET_ALL_GENRES_OPTIONS', '/genres', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('GET_ALBUMS_BY_RANKING_OPTIONS', '/albums/ranking/:ranking', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('GET_FAVORITE_ALBUMS_OPTIONS', '/albums/favorites/:user_id', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('GET_ALL_OPTIONS', '/albums/ranking/:limit', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('PUT_LIKE_FOR_USER_OPTIONS', '/albums', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('DELETE_LIKE_FOR_USER_OPTIONS', '/albums/delete', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('POST_ALBUM_OPTIONS', '/albums', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('RESET_ALBUM_OPTIONS', '/albums/reset', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

    conf = {
        'global':{
            'server.socket_host' : 'student04.cse.nd.edu',
            'server.socket_port' : port,
            },
        '/' : {'request.dispatch' : dispatcher,
                'tools.CORS.on': True, 
            }
    }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"
    
if __name__ == '__main__':
    port = sys.argv[1] if len(sys.argv) > 1 else '51047'
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service(int(port))

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
