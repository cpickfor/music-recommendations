#!/usr/bin/env python3
import json

class album:
    def __init__ (self, line=None):
        if not line:
            self.__ranking   = 0
            self.__year      = 0
            self.__decade    = ''
            self.__title     = ''
            self.__artist    = ''
            self.__genres    = []
            self.__subgenres = []
        else:
            split_string     = line.split('::') 
            self.__ranking   = int(split_string[0])
            self.__year      = int(split_string[1])
            self.__decade    = str(self.__year // 10 * 10)[-2:]
            self.__title     = split_string[2].strip()
            self.__artist    = split_string[3].strip()
            self.__genres    = [x.strip() for x in split_string[4].split(',')]
            self.__subgenres = [x.strip() for x in split_string[5].split(',')]

    # returns integer
    def get_ranking(self):
        return self.__ranking

    # returns integer
    def get_year(self):
        return self.__year

    # returns string
    def get_decade(self):
        return self.__decade

    # returns string
    def get_artist(self):
        return self.__artist

    # returns string
    def get_genres(self):
        return self.__genres

    # returns string
    def get_subgenres(self):
        return self.__subgenres
    
    # returns title
    def get_title(self):
        return self.__title

    def set_ranking(self, ranking):
        self.__ranking = ranking

    def set_year(self, year):
        self.__year = year

    def set_decade(self, decade):
        self.__decade = decade

    def set_artist(self, artist):
        self.__artist = artist
    
    def set_title(self, title):
        self.__title = title

    # returns string
    def set_genres(self, genres):
        self.__genres = genres

    # returns string
    def set_subgenres(self, subgenres):
        self.__subgenres = subgenres

    def __str__ (self):
        return """Title: {}
Ranking: {}
Year: {}
Decade: {}s
Artist: {}
Genres: {}
Subgenres: {}""".format(
            self.__title,
            self.__ranking,
            self.__year,
            self.__decade,
            self.__artist,
            ', '.join(self.__genres),
            ', '.join(self.__subgenres)
        )

    def __repr__(self):
        return 'album({})'.format(self.__dict__)

    def get_json(self):
        album_stuff = {}
        album_stuff['ranking'] = self.__ranking
        album_stuff['year'] = self.__year
        album_stuff['decade'] = self.__decade
        album_stuff['artist'] = self.__artist
        album_stuff['genres'] = self.__genres
        album_stuff['subgenres'] = self.__subgenres
        album_stuff['title'] = self.__title
        return album_stuff

if __name__=='__main__':
    f = open('albums.dat')
    line = f.readlines()[1]
    a = album(line)
    print(a)
    print(a.get_json())
    f.close()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
