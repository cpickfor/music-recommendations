#!/usr/bin/env python3
import json

from album import album

class _album_database():
    
    def __init__(self):
        self.albums = {}
        self.favorites = {}
        self.max_ranking = 0

    def load_albums(self, albums_file):
        f = open(albums_file)
        next(f)
        for line in f:
                Album = album(line)
                self.albums[Album.get_ranking()] = Album
                self.max_ranking = max(Album.get_ranking(), self.max_ranking)
        f.close()

    def SORT_BY(self, member, reverse=False):
        return sorted((album for album in self.albums.values()),
                key=lambda album: getattr(album, 'get_' + member)(),
                reverse=reverse)

    def GET_ALL_ARTISTS(self):
        return set(album.get_artist() for album in self.albums.values())

    def GET_ALL_GENRES(self):
        return set(genre for album in self.albums.values() for genre in album.get_genres())

    def GET_ALL_SUBGENRES(self):
        return set(genre for album in self.albums.values() for genre in album.get_subgenres())

    def GET_ALBUMS_BY_ARTIST(self,artist):
        return (album for album in self.albums.values() if album.get_artist() == artist)
    
    def GET_ALBUMS_BY_YEAR(self,year):
        return (album for album in self.albums.values() if album.get_year() == int(year))

    def GET_ALBUMS_BY_RANKING(self,ranking):
        return (album for album in self.albums.values() if album.get_ranking() == int(ranking))

    def GET_ALBUMS_BY_RANK_LIMIT(self,ranking):
        return (album for album in self.albums.values() if album.get_ranking() <= int(ranking))
            
    def GET_ALBUMS_BY_DECADE(self,decade):
        return (album for album in self.albums.values() if album.get_decade() == decade)

    def GET_ALBUMS_BY_GENRE(self, genre):
        return (album for album in self.albums.values() if genre in album.get_genres())
    
    def DELETE_LIKED_ALBUM(self, album_title, user_id):
        if user_id in self.favorites:
            del self.favorites[user_id][album_title]

    # user_id should be a string
    def PUT_LIKED_ALBUM(self, album_title, user_id):
        if user_id not in self.favorites:
            self.favorites[user_id] = set([album_title])
        else:
            self.favorites[user_id].add(album_title)

    def ADD_ALBUM(self, album_json):
        album_data = json.loads(album_json)

        # assign a new ranking to  be one higher than the maximum
        album_data['ranking'] = self.max_ranking + 1
        self.max_ranking = self.max_ranking + 1

        print(self.max_ranking)
        Album = album()

        Album.set_ranking(int(album_data['ranking']))
        Album.set_year(int(album_data['year']))
        Album.set_decade(str(album_data['decade']))
        Album.set_title(album_data['title'])
        Album.set_artist(album_data['artist'])
        Album.set_genres(album_data['genres'])
        Album.set_subgenres(album_data['subgenres'])
        self.albums[Album.get_ranking()] = Album


if __name__ == "__main__":
    adb = _album_database()

    adb.load_albums('albums.dat')
    print(adb.GET_ALL_GENRES())
    print(adb.GET_ALL_SUBGENRES())

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
