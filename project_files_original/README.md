## Description

## Running tests

To run the unit tests, run `./test_api.py`. Specify the `-v` option to see each test individually.

How API can and should be used:
Our API is made up of an album class that contains all the properties of the album.  Basically, this class will parse the lines of the file.  This file determines the printing form of the file data.  This class also contains the get methods that are called in the album database file to get specific attributes of each line of the data file.  The album database contains all the get methods that will be called when querying the data file.  By calling the methods in the album database in accordance with the user input, we can find all the albums and their properties that correspond to the search term that the user provides and return these files as a recommendation of what album the user might want to listen to according to their search.  These are also sorted by rank so that the user has an idea of the popularity of the album that they are being recommended.  

Webservice Port: 51064
How webservice can and should be used:
Our webservice is can and should be used to help a user find and filter music albums by accessing information on the top 500 albums as chosen by Rolling Stone Magazine.  The user has access to filtering by ranking, artist, genre, subgenre, decade, and year.  The webservice also allows for the user to add their own favorite albums to the list by utilizing the post album method. We also decided to create a method for the user to like albums.  This allows the user to keep track of the music that they appreciate and then be able to display all the albums they've liekd as well.