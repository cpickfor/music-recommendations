//// CONSTANTS ////

const BASE_URL = 'http://student04.cse.nd.edu:51047';

const $albumsList = document.getElementById('albums-list');
const $artistsButton = document.getElementById('artistsButton');
const $genresButton = document.getElementById('genresButton');
const $searchButton = document.getElementById('searchButton');
const $searchText = document.getElementById('query');
const $parameterDropdown = document.getElementById('Dropdown');
const $favoritesButton = document.getElementById('favoritesButton');
const $usernameText = document.getElementById('username');


//// HELPER FUNCTIONS ////

function emptyNode($node) {
    // Clear all contents to replace them
    while ($node.firstChild) {
        $node.removeChild($node.firstChild);
    }
}

function makeAlbumList(response) {
    emptyNode($albumsList);

    if (response.result === 'success') {
        response.albums.forEach(album => {
            const $li = document.createElement('li');
            $li.textContent = `${album.title} — ${album.artist} (${album.year})`;
            $albumsList.appendChild($li);

            const $button = document.createElement('button');
            $button.textContent = 'Like';
            $button.addEventListener('click', event => {
                putLike(event, album.title);
            });
            $li.appendChild($button);
	    
            const $button1 = document.createElement('button');
            $button1.textContent = 'Unlike';
            $button1.addEventListener('click', event => {
                deleteLike(event, album.title);
            });
            $li.appendChild($button1);

        })
    } else {
        $albumsList.textContent = 'No results found';
    }
}

function displayArtistsList(response){
    emptyNode($albumsList);

	if(response.result === 'success'){
		response.artists.forEach(artist => {
			const $li = document.createElement('li');
			$li.textContent = `${artist}`;
			$albumsList.appendChild($li);
		})
	} else {
		$albumsList.textContent = 'No results found';
	}

}

function displayGenresList(response){
    emptyNode($albumsList);

	if(response.result === 'success'){
		response.genres.forEach(genre => {
			const $li = document.createElement('li');
			$li.textContent = `${genre}`;
			$albumsList.appendChild($li);
		})
	} else {
		$albumsList.textContent = 'No results found';
    }
}

function displayFavoritesList(response){
    emptyNode($albumsList);

	if(response.result === 'success'){
		response.albums.forEach(album => {
			const $li = document.createElement('li');
			$li.textContent = `${album}`;
			$albumsList.appendChild($li);
		})
	} else {
		$albumsList.textContent = 'No results found';
    }

}

function putLike(event, albumTitle) {
    const payload = {
        title: albumTitle,
        user_id: $usernameText.value || 'Anonymous',
    };

    fetch(
        BASE_URL + '/albums',
        {
            method: 'PUT',
            body: JSON.stringify(payload),
        }
    )
        .then(response => response.json())
        .then(response => {
            console.log(response);
            if (response.result == 'success') {
                event.target.innerText = 'Liked';
                event.target.disabled = true;
            } else {
                event.target.innerText = 'Like failed';
            }
        });
}

function deleteLike(event, albumTitle) {
    const payload = {
        title: albumTitle,
        user_id: $usernameText.value || 'Anonymous',
    };

    fetch(
        BASE_URL + '/albums/delete',
        {
            method: 'PUT',
            body: JSON.stringify(payload),
        }
    )
        .then(response => response.json())
        .then(response => {
            console.log(response);
            if (response.result == 'success') {
                event.target.innerText = 'Liked';
                event.target.disabled = true;
            } else {
                event.target.innerText = 'Like failed';
            }
        });
}


// Test data
// response = {
    // "result": "success", "albums": [{"ranking": 1, "year": 1967, "decade": "60", "artist": "The Beatles", "genres": ["Rock"], "subgenres": ["Rock & Roll", "Psychedelic Rock"], "title": "Sgt. Pepper's Lonely Hearts Club Band"}, {"ranking": 3, "year": 1966, "decade": "60", "artist": "The Beatles", "genres": ["Rock"], "subgenres": ["Psychedelic Rock", "Pop Rock"], "title": "Revolver"}, {"ranking": 5, "year": 1965, "decade": "60", "artist": "The Beatles", "genres": ["Rock", "Pop"], "subgenres": ["Pop Rock"], "title": "Rubber Soul"}, {"ranking": 10, "year": 1968, "decade": "60", "artist": "The Beatles", "genres": ["Rock"], "subgenres": ["Rock & Roll", "Pop Rock", "Psychedelic Rock", "Experimental"], "title": "The Beatles (The White Album)"}, {"ranking": 14, "year": 1969, "decade": "60", "artist": "The Beatles", "genres": ["Rock"], "subgenres": ["Psychedelic Rock", "Classic Rock", "Pop Rock"], "title": "Abbey Road"}, {"ranking": 39, "year": 1963, "decade": "60", "artist": "The Beatles", "genres": ["Rock"], "subgenres": ["Beat", "Rock & Roll"], "title": "Please Please Me"}, {"ranking": 53, "year": 1964, "decade": "60", "artist": "The Beatles", "genres": ["Rock"], "subgenres": ["Beat", "Rock & Roll"], "title": "Meet The Beatles!"}, {"ranking": 307, "year": 1964, "decade": "60", "artist": "The Beatles", "genres": ["Rock", "Stage & Screen"], "subgenres": ["Soundtrack", "Beat", "Pop Rock"], "title": "A Hard Day's Night"}, {"ranking": 331, "year": 1965, "decade": "60", "artist": "The Beatles", "genres": ["Rock", "Stage & Screen"], "subgenres": ["Beat", "Soundtrack", "Pop Rock"], "title": "Help!"}, {"ranking": 392, "year": 1970, "decade": "70", "artist": "The Beatles", "genres": ["Rock"], "subgenres": ["Pop Rock"], "title": "Let It Be"}]
// };
// makeAlbumList(response);


//// ON LOAD ////

fetch(BASE_URL + '/albums')
    .then(response => response.json())
    .then(makeAlbumList);


//// EVENT LISTENERS ////

$searchButton.addEventListener('click', () => {
    console.log($parameterDropdown.value)
    const search = $searchText.value;
    const url = search ? `${BASE_URL}/albums/${$parameterDropdown.value}/${encodeURIComponent(search)}` : `${BASE_URL}/albums`;

    fetch(url)
        .then(response => response.json())
        .then(makeAlbumList);
});

$artistsButton.addEventListener('click', () => {
	const url = `${BASE_URL}/artists`;
	fetch(url)
		.then(response => response.json())
		.then(displayArtistsList);
});

$genresButton.addEventListener('click', () => {
	const url = `${BASE_URL}/genres`;
	fetch(url)
		.then(response => response.json())
		.then(displayGenresList);
});

$favoritesButton.addEventListener('click', () => {
	const url = `${BASE_URL}/albums/favorites/${$usernameText.value || 'Anonymous'}`;
	fetch(url)
		.then(response => response.json())
		.then(displayFavoritesList);
});
